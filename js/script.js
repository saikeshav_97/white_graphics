/**
 * scripts.js
 * Contains Script for basic static website named "White Graphics"
 */
$(window).on('load',function(){
    $("#preloader").delay(5000).fadeOut('slow');
    
    /*Team Section*/
    $("#team-members").owlCarousel({
        items: 2,
        autoplay: true,
        loop: true,
        nav: true,
        navText: ['<i class="lni lni-chevron-left-circle"></i>', '<i class="lni lni-chevron-right-circle"></i>'],
        dots: false,
        smartSpeed: 700,
        margin: 20
    });
    
    /*Progress Bar*/
    $("#progress-elements").waypoint(function() {
        $(".progress-bar").each(function() {
            $(this).animate({
                width: $(this).attr("aria-valuenow")+"%"
            }, 800);
        });
        
        this.destroy(); //deletes the waypoint object binded with progress-elem,ents
        // so that it only animates once.
    }, {
        offset: 'bottom-in-view'
    })
});